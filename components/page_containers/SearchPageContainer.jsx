const dataDrugs = [
  "Amlodipine",
  "Amoxcillin",
  "Calcium",
  "Codeine",
  "Dexamethasone",
  "Dophamine",
  "Factor IX Complex",
  "Hydromorphone",
  "Hydrocortisone",
  "Isotretinoin",
  "Meloxicam",
  "Oxycodone",
  "Prednisone",
  "Ranitidine",
  "Sucralfate",
];

function SearchPageContainer() {
  return (
    <>
      <h2>SEARCH</h2>
      <div className="form-group">
        <label className="form-label">Type a drug name: </label>
        <input
          type="search"
          className="form-control"
          placeholder="e.g. amlodipine"
        />
      </div>

      {dataDrugs.map((drug, idx) => (
        <p key={idx}>{drug}</p>
      ))}
    </>
  );
}

export default SearchPageContainer;
