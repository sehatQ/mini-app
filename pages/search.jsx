import MainLayout from "../components/layouts/MainLayout";
import SearchPageContainer from "../components/page_containers/SearchPageContainer";

function Search() {
  return (
    <MainLayout>
      <SearchPageContainer />
    </MainLayout>
  );
}

export default Search;
