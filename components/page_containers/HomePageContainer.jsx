import UserCard from "../cards/UserCard";

function HomePageContainer() {
  return (
    <>
      <h2>HOME</h2>

      <UserCard
        photo="https://reqres.in/img/faces/7-image.jpg"
        name="Lindsey Ferguson"
      />

      <UserCard
        photo="https://reqres.in/img/faces/2-image.jpg"
        name="Emily Mewis"
      />
    </>
  );
}

export default HomePageContainer;
