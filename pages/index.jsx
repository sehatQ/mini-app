import MainLayout from "../components/layouts/MainLayout";
import HomePageContainer from "../components/page_containers/HomePageContainer";
import { initializeStore } from "../store";

function Index() {
  return (
    <MainLayout>
      <HomePageContainer />
    </MainLayout>
  );
}

export async function getServerSideProps() {
  const reduxStore = initializeStore();
  const { dispatch } = reduxStore;

  return { props: { initialReduxState: reduxStore.getState() } };
}

export default Index;
