import { useEffect, useRef } from "react";

function AboutPageContainer() {
  const imgRef = useRef(null);

  useEffect(() => {
    const timeout = simulateDelayImage();

    return () => {
      clearTimeout(timeout);
    };
  }, []);

  function simulateDelayImage() {
    return setTimeout(() => {
      imgRef.current.src =
        "https://images.pexels.com/photos/269077/pexels-photo-269077.jpeg?cs=srgb&dl=pexels-pixabay-269077.jpg&fm=jpg";
    }, 5000);
  }

  return (
    <>
      <h2>ABOUT US</h2>
      <div>
        <img ref={imgRef} alt="about-us" className="img" />
      </div>
      <div>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum
        </p>
      </div>
    </>
  );
}

export default AboutPageContainer;
