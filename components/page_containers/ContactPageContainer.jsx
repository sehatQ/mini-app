function ContactPageContainer() {
  return (
    <>
      <h2>CONTACT US</h2>
      <div className="alert alert-info">
        <p>Successfully submitted.</p>
      </div>
      <form autoComplete="off">
        <div className="form-group">
          <label htmlFor="name" className="form-label">
            Name:
          </label>
          <input type="text" name="name" id="name" className="form-control" />
          <div className="invalid-feedback">This field cannot be blank</div>
        </div>
        <div className="form-group">
          <label htmlFor="email" className="form-label">
            Email:
          </label>
          <input
            type="email"
            name="email"
            id="email"
            className="form-control"
          />
          <div className="invalid-feedback">This field cannot be blank</div>
        </div>
        <div className="form-group">
          <label htmlFor="type" className="form-label">
            Type of message:
          </label>
          <select name="type" id="type" className="input-select form-control">
            <option value="" disabled>
              -- select --
            </option>
            <option value="inquiry">Inquiry</option>
            <option value="offer">Offer</option>
            <option value="other">Other</option>
          </select>
          <div className="invalid-feedback">This field cannot be blank</div>
        </div>
        <div className="form-group">
          <label htmlFor="message" className="form-label">
            Message:
          </label>
          <textarea name="message" id="message" className="form-control" />
          <div className="invalid-feedback">This field cannot be blank</div>
        </div>
        <button className="button button-primary button-block">Submit</button>
      </form>
    </>
  );
}

export default ContactPageContainer;
