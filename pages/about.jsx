import MainLayout from "../components/layouts/MainLayout";
import AboutPageContainer from "../components/page_containers/AboutPageContainer";

function About() {
  return (
    <MainLayout>
      <AboutPageContainer />
    </MainLayout>
  );
}

export default About;
