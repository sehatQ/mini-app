function UserCard({ photo, name }) {
  return (
    <div className="user-card">
      <img src={photo} alt={name} className="avatar" width={50} height={50} />
      <div className="user-card-body">
        <p>{name}</p>
      </div>
    </div>
  );
}

export default UserCard;
