import MainLayout from "../components/layouts/MainLayout";
import ContactPageContainer from "../components/page_containers/ContactPageContainer";

function Contact() {
  return (
    <MainLayout>
      <ContactPageContainer />
    </MainLayout>
  );
}

export default Contact;
