import Head from "next/head";
import Link from "next/link";
import { FaHome, FaPaperPlane, FaInfo, FaBars, FaSearch } from "react-icons/fa";

function MainLayout({ children }) {
  return (
    <>
      <Head>
        <title>SehatQ Mini App</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap"
          rel="stylesheet"
        />
      </Head>
      <header>
        <nav className="navbar">
          <ul className="navbar-nav">
            <li className="nav-item">
              <FaBars />
            </li>
            <li className="nav-item">
              <Link href="/">
                <span className="nav-brand">
                  SehatQ <sup>Mini App</sup>
                </span>
              </Link>
            </li>
          </ul>
        </nav>
      </header>
      <main>{children}</main>
      <footer>
        <nav className="navbar-bottom">
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link href="/">
                <a>
                  <FaHome />
                </a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/contact">
                <a>
                  <FaPaperPlane />
                </a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/search">
                <a>
                  <FaSearch />
                </a>
              </Link>
            </li>
            <li className="nav-item">
              <Link href="/about">
                <a>
                  <FaInfo />
                </a>
              </Link>
            </li>
          </ul>
        </nav>
      </footer>
    </>
  );
}

export default MainLayout;
